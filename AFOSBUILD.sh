rm -rf /opt/ANDRAX/dotnet

if [ $(uname -m | grep 'x86_64') ]; then
   wget https://download.visualstudio.microsoft.com/download/pr/e89c4f00-5cbb-4810-897d-f5300165ee60/027ace0fdcfb834ae0a13469f0b1a4c8/dotnet-sdk-3.1.426-linux-x64.tar.gz

   if [ $? -eq 0 ]
   then
     # Result is OK! Just continue...
     echo "Download dotnet-sdk... PASS!"
   else
     # houston we have a problem
     exit 1
   fi

else
   wget https://download.visualstudio.microsoft.com/download/pr/79f1cf3e-ccc7-4de4-9f4c-1a6e061cb867/68cab78b3f9a5a8ce2f275b983204376/dotnet-sdk-3.1.426-linux-arm64.tar.gz
   if [ $? -eq 0 ]

   then
     # Result is OK! Just continue...
     echo "Download dotnet-sdk... PASS!"
   else
     # houston we have a problem
     exit 1
   fi

fi

mkdir dotnet

tar -xvzf dotnet-sdk*.tar.gz -C dotnet

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Extract dotnet-sdk... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf dotnet /opt/ANDRAX

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
